/*  
  file: services.js
  description: this file contains all services of the DurgaPuja app.
*/
angular.module('durgaPuja.services', [])

// UserService is an example of service using localStorage 
// to store user credentials.
.service('UserService', function() {
  // For the purpose of this example I will store user data on ionic local storage but you should save it on a database
  var setUser = function(user_data) {
    window.localStorage.user = JSON.stringify(user_data);    
  };

  var getUser = function(){
    return JSON.parse(window.localStorage.user || '{}');
  };

  var logoutUser = function(){
    window.localStorage.removeItem('user');
    console.log(window.localStorage.getItem('user'));
  };

  return {
    getUser: getUser,
    setUser: setUser,
    logoutUser: logoutUser
  };
});