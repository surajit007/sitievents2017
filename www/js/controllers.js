/*  
  file: controllers.js
  description: this file contains all controllers of the DurgaPuja app.
*/

//controllers are packed into a module
angular.module('durgaPuja.controllers', [])

//top view controller
.controller('AppController', ['$scope','$state','CartService','BackendService','$ionicPlatform','$ionicHistory','$cordovaToast','UserService','$ionicLoading','$timeout','envService','$cordovaInAppBrowser','$rootScope','$ionicPopup', function($scope, $state, CartService,BackendService,$ionicPlatform,$ionicHistory,$cordovaToast,UserService,$ionicLoading,$timeout,envService,$cordovaInAppBrowser,$rootScope,$ionicPopup) {
  
  // #SIMPLIFIED-IMPLEMENTATION:
  // Simplified handling and logout function.
  // A real app would delegate a service for organizing session data
  // and auth stuff in a better way.
  
  //$rootScope.user = {};
 
  //$scope.cart = CartService.loadCart();
  //$scope.getTotal = CartService.getTotal; 
  $scope.user = UserService.getUser('facebook');

  $scope.logout = function(){    
    //$rootScope.user = {};
    if($scope.user.userID) {
      $ionicLoading.show();
      // Facebook logout
        facebookConnectPlugin.logout(function(){
          $ionicLoading.hide();          
        },
        function(fail){
          $ionicLoading.hide();
        });
    }
    UserService.logoutUser();        
    localStorage.removeItem("loggedIn");                
    $ionicHistory.clearHistory();
    $ionicHistory.removeBackView();
    $ionicHistory.nextViewOptions({ 
      disableBack: true,                     
      historyRoot: true
    });
    $state.go('start')
  };

  $scope.openFaq = function(){ 
    var options = {
      title: "FAQ"
    }  
    cordova.plugins.SitewaertsDocumentViewer.viewDocument('file:///android_asset/www/pdf/FAQ.pdf', 'application/pdf', options);
  };

  var countTimerForCloseApp = false;
    /*$ionicPlatform.registerBackButtonAction(function(e) {
       e.preventDefault();
       
       function showConfirm() {
        var confirmPopup = $ionicPopup.show({
         title : 'Exit AppName?',
         template : 'Are you sure you want to exit AppName?',
         buttons : [{
          text : 'Cancel',
          type : 'button-royal button-outline',
         }, {
          text : 'Ok',
          type : 'button-royal',
          onTap : function() {
           ionic.Platform.exitApp();
          }
         }]
        });
       };

       function showConfirmCordova() {
        if (countTimerForCloseApp) {
         ionic.Platform.exitApp();
        } else {
         countTimerForCloseApp = true;
          $cordovaToast
          .show('Press again to exit.', 3000, 'bottom')
          .then(function(success) {
            // success
          }, function (error) {
            // error
          });         
         $timeout(function() {
          countTimerForCloseApp = false;
         }, 3000);
        }

       };

       // Is there a page to go back to?
       //console.log(JSON.stringify($ionicHistory.viewHistory()));
       if ($ionicHistory.backView()) {
        // Go back in history
        $ionicHistory.backView().go();
       } else {
        // This is the last page: Show confirmation popup
        var isWebView = ionic.Platform.isWebView();
        if(isWebView){
          showConfirmCordova();
        }else{
          showConfirm();
        }
        
       }

       return false;
      }, 101);*/

}])

// This controller is bound to the "app.home" view
.controller('HomeCtrl', ['$scope','$state','BackendService','$ionicLoading','$ionicPopup','$ionicPlatform', function($scope, $state, BackendService,$ionicLoading,$ionicPopup,$ionicPlatform) {
    $ionicPlatform.ready(function() { 
      BackendService.getText()
        .then(function(data) {          
              $scope.footerText = data[0].returnMessage;
              $scope.showMarquee = true;
          }, function(error) {
              //console.log(error);
              $ionicPopup.alert({
                 title: 'Error',
                 template: error            
              });          
      }); 
    });
}])