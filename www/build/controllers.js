angular.module('durgaPuja.controllers')
// This controller is bound to the "app.account" view
.controller('AccountCtrl', function($scope) {
    
    //readonly property is used to control editability of account form
    $scope.readonly = true;
  
    // #SIMPLIFIED-IMPLEMENTATION:
    // We act on a copy of the root user
    $scope.accountUser = JSON.parse(localStorage.getItem("user"));
    $scope.user = angular.copy($scope.accountUser);
    //console.log($scope.accountUser);
    var userCopy = {};
  
    $scope.startEdit = function(){
      $scope.readonly = false;
      userCopy = angular.copy($scope.user);    
    };
  
    $scope.cancelEdit = function(){
      $scope.readonly = true;
      $scope.user = userCopy;
    };
    
    // #SIMPLIFIED-IMPLEMENTATION:
    // this function should call a service to update and save 
    // the data of current user.
    // In this case we'll just set form to readonly and copy data back to $rootScope.
    $scope.saveEdit = function(){
      $scope.readonly = true;
      localStorage.setItem("user",JSON.stringify($scope.accountUser));     
    };
})
angular.module('durgaPuja.controllers')
// Shop controller.
.controller('ShopPujaCtrl', function($scope, $stateParams, $cordovaToast, BackendService, CartService, envService, $rootScope,$q,$ionicPopup,$filter,$ionicLoading,$timeout,$cordovaNetwork) {

  $scope.getAllPuja = function(){
    BackendService.getPujaList()
    .then(function(data) {
          //console.log(data);
          //$rootScope.products = data;
          $scope.products = data;
          $ionicLoading.hide();
          CartService.saveAllPuja(data);
      }, function(error) {
        $ionicLoading.hide();
          $ionicPopup.alert({
               title: 'Error',
               template: error
             });
      });
  };

  $scope.motionFab = function(id) {
        var type = 'drop';
        var shouldAnimate = false;
        var fab = document.getElementById('puja_'+id);
        var classes = type instanceof Array ? type : [type];
        for (var i = 0; i < classes.length; i++) {
            fab.classList.toggle(classes[i]);
            shouldAnimate = fab.classList.contains(classes[i]);
            if (shouldAnimate) {
                (function(theClass) {
                    $timeout(function() {
                        fab.classList.toggle(theClass);
                    }, 300);
                })(classes[i]);
            }
        }
    };


  // private method to add a product to cart
  var addProductToCart = function(product){
    product.Quantity = 1;
    $scope.cart.products.push(product);
    CartService.saveCart($scope.cart);
  };

  $scope.saveVote = function(id){
     $scope.motionFab(id);
     $timeout(function() {
      BackendService.saveVote(id)
      .then(function(data) {
        if (data[0].returnMessage === 'TRUE') {
           $ionicPopup.alert({
            title: 'Vote Puja.',
            subTitle: 'Vote completed successfully.'
          }).then(function(res) {
                $("#puja_"+id).prop("disabled",true);
           });
        } else {
            $ionicPopup.alert({
              title: 'Vote Puja.',
              subTitle: 'Pls try again.'
          });
        }
       },
       function(error) {
        console.log('error occured');
      });
     }, 400);

    }

  $ionicLoading.show();
  $scope.getAllPuja();

})

angular.module('durgaPuja.controllers')
// Shop controller.
.controller('ShopPujaDetailCtrl', function($scope, $stateParams, $ionicActionSheet, BackendService, CartService, envService, $rootScope, $q, $ionicPopup, $filter,$timeout) {
  
  // In this example feeds are loaded from a json file.
  // (using "getProducts" method in BackendService, see services.js)
  // In your application you can use the same approach or load 
  // products from a web service.
  
  //using the CartService to load cart from localStorage   

  $scope.cart = CartService.loadCart();

  $scope.allPuja = CartService.loadAllPuja();

  
  $scope.product = $filter('filter')($scope.allPuja, {PujaID: $stateParams.productId })[0];

  $scope.product.Quantity = $stateParams.quantity; 

  $scope.loadMap = function(){
    var latLng = new google.maps.LatLng($scope.product.PujaLatitude, $scope.product.PujaLongitude);

    //var latLng = new google.maps.LatLng(37.3000, -120.4833);

    var mapOptions = {
      center: latLng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };    
 
    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

    //Wait until the map is loaded
    google.maps.event.addListenerOnce($scope.map, 'idle', function(){   
      
      var marker = new google.maps.Marker({
          map: $scope.map,
          animation: google.maps.Animation.DROP,
          position: latLng
      });      
     
      var infoWindow = new google.maps.InfoWindow({
          content: $scope.product.PujaName
      });
     
      google.maps.event.addListener(marker, 'click', function () {
          infoWindow.open($scope.map, marker);
      });
   
    });
  } 

  $timeout($scope.loadMap);   

  // private method to add a product to cart
  var addProductToCart = function(product){
    $scope.cart.products.push(product);
    CartService.saveCart($scope.cart);
  };

  // method to add a product to cart via $ionicActionSheet
  $scope.addProduct = function(product){
    $ionicActionSheet.show({
       buttons: [
         { text: '<b>Add to cart</b>' }
       ],
       titleText: 'Buy Pass for ' + product.PujaName,
       cancelText: 'Cancel',
       cancel: function() {
          // add cancel code if needed ..
       },
       buttonClicked: function(index) {
         if(index == 0){
           addProductToCart(product);
           return true;
         }
         return true;
       }
     });
  };

})
angular.module('durgaPuja.controllers')

.controller('UserCtrl', function ($scope, $state, $ionicPopup, CartService, $cordovaDialogs,
    BackendService,envService,$ionicLoading,$timeout,$ionicHistory,$q,UserService,$cordovaDevice) { 
   
     $scope.user = {};
   
     /*$scope.user = {email : "007.surajit@gmail.com",
                   password : "123"};*/
   
     // This is the success callback from the login method
     var fbLoginSuccess = function(response) {
       if (!response.authResponse){
          $ionicLoading.hide();
         fbLoginError("Cannot find the authResponse");
         return;
       }
   
       var authResponse = response.authResponse;
   
       getFacebookProfileInfo(authResponse)
       .then(function(profileInfo) {
         // For the purpose of this example I will store user data on local storage
         validateFaceBookCredentials(profileInfo,response);
       }, function(fail){
         // Fail get profile info
         console.log('profile info fail', fail);
         $ionicLoading.hide();
         $cordovaDialogs.alert(JSON.stringify(fail), 'Facebook Login Failed', 'Ok')
       }); 
     };
   
     // This is the fail callback from the login method
     var fbLoginError = function(error){
       console.log('fbLoginError', error);
       $ionicLoading.hide();
       $cordovaDialogs.alert(error, 'Facebook Login Failed', 'Ok')
     };
   
     // This method is to get the user profile info from the facebook api
     var getFacebookProfileInfo = function (authResponse) {
       var info = $q.defer();
   
       facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
         function (response) {
           console.log(response);
           info.resolve(response);
         },
         function (response) {
           console.log(response);
           info.reject(response);
         }
       );
       return info.promise;
     };

     var validateFaceBookCredentials = function(profileInfo,response){
        var userObj = {'yourName': profileInfo.name, 'yourEmail' : profileInfo.email,
        'facebookID' : profileInfo.userID, 'tokenKey' : localStorage.getItem('app-token'),               
        'IMEI' : $cordovaDevice.getUUID(), 'deviceOS' : $cordovaDevice.getPlatform()
      };
        BackendService.checkFBLogin(userObj)
        .then(function(data) {
			//console.log(JSON.stringify(data[0]));
              if(data[0].returnMessage === 'TRUE'){
                  // For the purpose of this example I will store user data on local storage
                UserService.setUser({
                  authResponse: response.authResponse,
                  userID: profileInfo.id,
                  name: profileInfo.name,
                  email: profileInfo.email,
                  picture : "https://graph.facebook.com/" + response.authResponse.userID + "/picture?type=large"
                });
                localStorage.setItem("loggedIn","true");
                $ionicHistory.clearHistory();
                $ionicHistory.removeBackView();
                $ionicHistory.nextViewOptions({ 
                  disableBack: true,                     
                  historyRoot: true
                }); 
				$ionicLoading.hide();
                $state.go('app.home');
              }else{
				$ionicLoading.hide();
                $cordovaDialogs.alert(data[0].LoginName, 'Facebook Login Failed', 'Ok');
                // Clear the facebook login
                facebookConnectPlugin.logout(null,null);
              }
        });
     }
   
     //This method is executed when the user press the "Login with facebook" button
     $scope.facebookSignIn = function() {
       facebookConnectPlugin.getLoginStatus(function(success){
         if(success.status === 'connected'){
           // The user is logged in and has authenticated your app, and response.authResponse supplies
           // the user's ID, a valid access token, a signed request, and the time the access token
           // and signed request each expire
           console.log('getLoginStatus', success.status);
   
           // Check if we have our user saved
           var user = UserService.getUser('facebook');
   
           if(!user.userID){
             getFacebookProfileInfo(success.authResponse)
             .then(function(profileInfo) {
                validateFaceBookCredentials(profileInfo,success);               
             }, function(fail){
               // Fail get profile info
               console.log('profile info fail', fail);
             });
           }else{
             //alert('connected');
             // Already authenticated Facebook user
             localStorage.setItem("loggedIn","true");
             console.log(JSON.stringify(user));
             $ionicHistory.clearHistory();
             $ionicHistory.removeBackView();
             $ionicHistory.nextViewOptions({ 
               disableBack: true,                     
               historyRoot: true
             });
             $state.go('app.home');
           }
         } else {
           // If (success.status === 'not_authorized') the user is logged in to Facebook,
           // but has not authenticated your app
           // Else the person is not logged into Facebook,
           // so we're not sure if they are logged into this app or not.
   
           console.log('getLoginStatus', success.status);
   
           $ionicLoading.show({
             template: 'Logging in...'
           });
   
           // Ask the permissions you need. You can learn more about
           // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
           facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
         }
       });
     };
   
   
     $scope.login = function(user){
       
       $ionicLoading.show();
   
       $scope.user = angular.copy(user);
       //in this case we just set the user in $rootScope
       var userObj = {'user_id': $scope.user.email, 'user_password' : $scope.user.password,
       'IMEI' : $cordovaDevice.getUUID()};
   
       $timeout( function(){ 
           BackendService.checkLogin(userObj)
              .then(function(data) {
				  console.log(JSON.stringify(data));
                   if(data[0].returnMessage === 'TRUE'){
                     UserService.logoutUser();            
                     localStorage.setItem("loggedIn","true");                
                     /*$rootScope.user = {
                         email : $scope.user.email,
                         name : data.LoginName,
                         phone : data.PhoneNumber                
                       };*/
                       UserService.setUser({
                         email : $scope.user.email,
                         name : data[0].LoginName,
                         phone : data[0].PhoneNumber                
                       });
                       //localStorage.setItem("user",JSON.stringify($rootScope.user));
                       loginForm.reset(); 
                       $ionicHistory.clearHistory();
                       $ionicHistory.removeBackView();
                       $ionicHistory.nextViewOptions({ 
                         disableBack: true,                     
                         historyRoot: true
                       });
                       //finally, we route our app to the 'app.shop' view
                       $state.go('app.home');
                       $ionicLoading.hide();
                     }else{
                       $ionicLoading.hide();
                     if(ionic.Platform.isWebView()){              
                       $cordovaDialogs.alert(data[0].LoginName, 'Login Failed', 'Ok')
                         .then(function() {
                           // callback success
                         });
                       }else{

                        $ionicLoading.hide();                      
                         $ionicPopup.alert({
                          title: 'Login Failed',
                          template: data[0].LoginName
                        });
                       } 
                     }                                       
               }, function(error) {
                 console.log(error);
                 $ionicLoading.hide();
                   $ionicPopup.alert({
                        title: 'Login Failed',
                        template: 'Could not authenticate your credentials'
                      });          
               });
        }, 1500);        
     };
   
     $scope.signup = function(user){
   
       $ionicLoading.show();
       
       $scope.user = angular.copy(user);
       //in this case we just set the user in $rootScope
       var userObj = {'yourName': $scope.user.name, 'yourEmail' : $scope.user.email,
                      'yourPhone' : $scope.user.phone, 'yourPassword' : $scope.user.password,
                      'tokenKey' : localStorage.getItem('app-token'), 'RegType' : 'normal',
                      'IMEI' : $cordovaDevice.getUUID(), 'deviceOS' : $cordovaDevice.getPlatform()
                     };
   
       $timeout( function(){ 
         BackendService.registerUser(userObj)
          .then(function(data) {
               if(data[0].ReturnStatus === 'TRUE'){
                 localStorage.setItem("loggedIn","true");                
                 /*$rootScope.user = {
                     email : $scope.user.email,
                     name : "Surajit Sarkar"    `            
                   };*/
                   UserService.setUser({
                     email : $scope.user.email,
                     name : $scope.user.name,
                     phone : $scope.user.phone                                
                   });
                   //localStorage.setItem("user",JSON.stringify($rootScope.user)); 
                   //finally, we route our app to the 'app.shop' view
                   $ionicHistory.clearHistory();
                   $ionicHistory.removeBackView();
                   $ionicHistory.nextViewOptions({ 
                     disableBack: true,                     
                     historyRoot: true
                   });
                   $state.go('app.home');
                   $ionicLoading.hide();
                 }else{
                   $ionicLoading.hide();
                   $ionicPopup.alert({
                    title: 'Registration Failed',
                    template: data[0].ReturnMessage
                  });
                 }                                        
           }, function(error) {
               console.log(error);
               $ionicLoading.hide();
               $ionicPopup.alert({
                    title: 'Login Failed',
                    template: JSON.stringify(error)
                  });          
           });
         }, 1500);     
     }
   
     $scope.recoverPassword = function(recoverEmail){
       
       BackendService.recoverPassword(recoverEmail)
         .then(function(data) {
           if (data[0].returnMessage === 'TRUE') {
             $ionicPopup.alert({
              title: 'Password reset email sent.',
              subTitle: 'Follow the directions in the email to reset your password.'           
             });
           } else {          
               $ionicPopup.alert({
                 title: 'Forgot password',
                 template: 'Recovery email not found.'            
             });
           }
         }, function(error) {
           console.log(error);
           $ionicPopup.alert({
             title: 'Forgot password',
             template: 'An error occured in recovering your password'
           });
         });
     }
     
})
angular.module('durgaPuja.controllers')
// Shop controller.
.controller('VoteResultsCtrl', function($scope, $stateParams, $ionicPopover, $cordovaToast, BackendService, CartService, envService, $rootScope, $q, $ionicPopup, $cordovaDialogs,UserService,$filter,$ionicLoading) {
  
  $scope.doRefresh = function(){    
    
    BackendService.getResults()
    .then(function(data) {          
           $scope.results = data; 
           $ionicLoading.hide();
          //console.log($scope.orders);       
      }, function(error) {
          $ionicLoading.hide();
          $ionicPopup.alert({
               title: error               
             });          
      });
  };
  
  $ionicLoading.show();
  $scope.doRefresh();    

})